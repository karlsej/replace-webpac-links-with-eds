# Replace WebPac Permalink with EDS Permalink (in Libguides or elsewhere) #
Convert webpac pro links to EDS links using php screen scraper
##Reason##
We have Libguides that feature lots of links to catalog records. The script converts those links on the fly to by sending the URL to a PHP script, which scrapes the screen and finds the record number, which it then converts into the EDS URL. It also checks if it is a netLibrary record, and if so, uses the URL in the webPAC record instead (EBSCO strips those records out).