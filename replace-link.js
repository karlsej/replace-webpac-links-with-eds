$('[href^="http://lois.losrios.edu/"]').each(function() {
  var loisRegex = /http:\/\/lois.losrios.edu\/($|search~\/[xy]$|screens)/i; // this matches LOIS home page - don't want to transform those.
  var URL = this.href;
  if (loisRegex.test(URL) === false) {
    this.href = encodeURIComponent(this.href).replace(/^/, 'http://scc.losrios.edu/~library/links/lois.php?url=');
  }
});